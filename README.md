# SEB Front-End 
## How to install
1. First install [node.js](http://nodejs.org/)
2. Navigate to application directory in cmd
3. Install required packages
	```code
	npm install
	```
## How to use
1. Navigate to application directory in cmd
2. Running application
	``` code
	npm stat
	```
3. Open in browser [localhost:3000](http://localhost:3000)
## Explanation of my choices
#### Front-End
##### Framework
For JavaScript framework i chose Angular.js because your are looking for programer who can work with Angular.js. Also it's
fun to write all templating, scripts and Back-End in same language

For CSS i decided not to use any libraries like Bootstrap or Semantic-UI because i want to show my skills and you can
create more beautiful and unique style.
##### Libraries
In Front-End for libraries i only chose "Font Awesome" because it has plenty of scalable and customizable icons.
##### Architecture
I made single page application, because it's simple project and you don't need any login or something.
Also applicaiton is RESTful for faster working.
#### Back-End
In Back-End i chose node.js because it's javascript based server.
##### Framework
express - Because it's easy to setup.
##### Libraries
nodemon - Because it allows faster development with node.js without needing constantly manually to restart server.

jade - Only simplifies html writing

node-sass-middleware - Because i hate to have Grunt or other tasks to compile css files even if i don't need it.

mongoose - Easy to work MongoDb adapter