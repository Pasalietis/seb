var express = require('express');
var router  = express.Router();
var Folder  = require('./../../models/folder.js');

var folders_cache = {};

function getFoldersByParentId (res, parent_id) {
	Folder.find({parent: parent_id}, function (err, folders) {
		if (err) {
			res.send(err);
		} else {
			folders_cache[parent_id] = folders;
			res.json(folders);
		}
	});
}

router.get('/:parent_id*?', function (req, res) {
	var parent_id;
	
	if (typeof req.params.parent_id !== 'undefined') {
		parent_id = req.params.parent_id;
	} else {
		parent_id = '';
	}
	
	if (typeof folders_cache[parent_id] === 'undefined') {
		getFoldersByParentId(res, parent_id);
	} else {
		res.json(folders_cache[parent_id]);
	}
});

router.post('/', function (req, res) {
	if (typeof req.body.name === 'undefined' || !req.body.name.length) {
		res.send(false);
	} else {
		if (typeof req.body.parent === 'undefined') {
			req.body.parent = '';
		}
		
		Folder.create({
			name:   req.body.name,
			parent: req.body.parent
		}, function (err) {
			if (err) {
				res.send(err);
			} else {
				getFoldersByParentId(res, req.body.parent);
			}
		});
	}
});

router.delete('/:folder_id', function (req, res) {
	var folder_id = req.params.folder_id;
	// clearing all cache
	folders_cache = {};
	
	Folder.findOneAndRemove({_id: folder_id}, function (err) {
		if (err) {
			res.send(err);
			return;
		}
		
		res.json(1);
	});
});


module.exports = router;
