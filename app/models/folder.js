var mongoose = require('mongoose');

var schema = new mongoose.Schema({
	name:   String,
	parent: String
});

schema.pre('remove', function (next) {
	this.constructor.find({parent: this._id}, function (err, folders) {
		if(err){
			return false;
		}
		
		for(var i = 0; i < folders.length; i++){
			folders[i].remove();
		}
		
		next();
	});
});

module.exports = mongoose.model('Folder', schema);