angular.module('foldersService', [])
	.factory('Folders', function ($http) {
		var api_link = '/api/folder';
		
		return {
			get:    function (id) {
				return $http.get(api_link + '/' + id);
			},
			create: function (folder_data) {
				return $http.post(api_link, folder_data);
			},
			delete: function (id) {
				return $http.delete(api_link + '/' + id);
			}
		}
	});