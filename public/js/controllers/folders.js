angular
	.module('foldersController', [])
	.controller('FoldersController', function ($scope, $http, Folders) {
		
		function getFolders () {
			var setting_loading = setTimeout(function () {
				$scope.loading_data = true;
			}, 100);
			
			var parent_folder = '';
			
			if ($scope.current_folder) {
				parent_folder = $scope.current_folder._id;
			}
			
			Folders.get(parent_folder).success(function (data) {
				clearTimeout(setting_loading);
				
				$scope.folders          = data;
				$scope.loading_data     = false;
				$scope.selected_folders = [];
			});
		}
		
		$scope.form_data        = {};
		$scope.back_trace       = [];
		$scope.add_folder       = false;
		$scope.loading_data     = null;
		$scope.current_folder   = null;
		$scope.form_data_errors = {};
		$scope.selected_folders = [];
		
		$scope.isLoading = function () {
			return is_loading;
		};
		
		$scope.open = function (folder) {
			$scope.back_trace.push(folder);
			$scope.current_folder = folder;
			getFolders();
		};
		
		$scope.back = function () {
			$scope.backTo($scope.back_trace[$scope.back_trace.length - 2]);
		};
		
		$scope.backTo = function (folder) {
			if (folder === $scope.current_folder) {
				return;
			}
			
			var index = $scope.back_trace.indexOf(folder);
			
			if (index >= 0) {
				$scope.current_folder = folder;
				$scope.back_trace     = $scope.back_trace.slice(0, index + 1);
			} else {
				$scope.current_folder = null;
				$scope.back_trace     = $scope.back_trace.slice(0, 1);
			}
			
			getFolders();
		};
		
		$scope.toggleAddFolder = function () {
			$scope.form_data        = {};
			$scope.add_folder       = !$scope.add_folder;
			$scope.form_data_errors = {};
		};
		
		$scope.addFolder = function () {
			var error = false;
			
			if (typeof $scope.form_data.name === 'undefined' || !$scope.form_data.name.length) {
				error = true;
				
				$scope.form_data_errors.name = true;
			} else {
				$scope.form_data_errors.name = false;
			}
			
			if (!error) {
				$scope.loading_data = true;
				
				var data = angular.copy($scope.form_data);
				
				if ($scope.current_folder) {
					data.parent = $scope.current_folder._id;
				}
				
				Folders.create(data)
					.success(function (data) {
						$scope.folders          = data;
						$scope.form_data        = {};
						$scope.loading_data     = false;
						$scope.form_data_errors = {};
					});
			}
		};
		
		$scope.selectFolder = function (folder) {
			var index = $scope.selected_folders.indexOf(folder);
			
			if (index > -1) {
				$scope.selected_folders.splice(index, 1);
			} else {
				$scope.selected_folders.push(folder);
			}
		};
		
		$scope.checkForAddFolder = function (e) {
			if (e.which == 13) {
				$scope.addFolder();
			}
		};
		
		getFolders();
		
		$scope.removeFolders = function () {
			var total = $scope.selected_folders.length;
			
			for (var i = 0; i < total; i++) {
				var folder = $scope.selected_folders[i];
				
				Folders.delete(folder._id)
					.success(function () {
						total--;
						
						if (total <= 0) {
							getFolders();
						}
					});
			}
		}
	});