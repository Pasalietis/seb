var express      = require('express');
var path         = require('path');
var favicon      = require('serve-favicon');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var mongoose     = require('mongoose');

var app      = express();
var database = require(path.join(__dirname, 'app', 'configs', 'database'));

mongoose.connect(database.url);

// view engine setup
app.set('views', path.join(__dirname, 'app', 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(require('node-sass-middleware')({
	src:         path.join(__dirname, 'public', 'src'),
	dest:        path.join(__dirname, 'public'),
	debug:       true,
	outputStyle: 'compressed'
}));
app.use(express.static(path.join(__dirname, 'public')));

require(path.join(__dirname, 'app', 'routes', 'router'))(app);

module.exports = app;
